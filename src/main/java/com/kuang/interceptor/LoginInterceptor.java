package com.kuang.interceptor;

import com.kuang.util.Const;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.Set;

/**
 * @Description:
 * @Author: StarSea99
 * @Date: 2020-08-30 11:04
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 去除要拦截的路径
        Set<String> uri = new HashSet();

        uri.add("/toLogin.htm");
        uri.add("/checkReader.do");
        uri.add("/toRegister.htm");
        uri.add("/submitAddReader.do");
        uri.add("/doLogin.do");

        //请求路径
        String servletPath = request.getServletPath();
        if(uri.contains(servletPath)){
            return true;
        }

        //2.判断用户是否登录 会员是否登录
        HttpSession session = request.getSession();
        Object admin = session.getAttribute(Const.ADMIN);
        Object reader = session.getAttribute(Const.READER);
        if(admin!=null || reader!=null){
            return true;
        }else{
            response.sendRedirect(request.getContextPath()+"/toLogin.htm");
            return false;
        }

    }
}
