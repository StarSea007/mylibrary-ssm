package com.kuang.service;

import com.kuang.pojo.Admin;

public interface AdminService {

    Admin select(Admin ad);

    void alterpwd(Admin ad);
}
