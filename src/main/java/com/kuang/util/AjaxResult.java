package com.kuang.util;

/**
 * @Description:
 * @Author: StarSea99
 * @Date: 2020-08-30 13:27
 */
public class AjaxResult {

    private boolean success;// 成功
    private String message;// 消息提示
    private String status;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
