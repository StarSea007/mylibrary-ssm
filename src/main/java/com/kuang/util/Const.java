package com.kuang.util;

/**
 * @Description: 常量
 * @Author: StarSea99
 * @Date: 2020-08-30 13:20
 */
public class Const {

    public static final String ADMIN = "admin";
    public static final String READER = "reader";
    public static final String CATEGORY = "category";
}
